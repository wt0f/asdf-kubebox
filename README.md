# bat asdf Plugin

![Build Status](https://gitlab.com/wt0f/asdf-kubebox/badges/master/pipeline.svg)

This is the plugin repo for [asdf-vm/asdf](https://github.com/asdf-vm/asdf.git)
to manage [astefanutti/kubebox](https://github.com/astefanutti/kubebox.git).

## Install

After installing [asdf](https://github.com/asdf-vm/asdf),
you can add this plugin like this:

```bash
asdf plugin add kubebox
asdf install kubebox 0.10.0
asdf global kubebox 0.10.0
`````