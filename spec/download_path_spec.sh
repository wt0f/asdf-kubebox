
export ASDF_INSTALL_VERSION=0.21.0
export ASDF_INSTALL_PATH=/tmp

Describe 'asdf-kubebox'
  Include 'bin/install'

  Describe 'get_download_url()'
    Context "when OS is OSX"
      Mock 'uname'
        echo "Darwin"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "x86_64"
      }

      It 'returns correct URL'
        When call get_download_url "${ASDF_INSTALL_VERSION}" 'kubebox'
        The output should equal "https://github.com/astefanutti/kubebox/releases/download/v${ASDF_INSTALL_VERSION}/kubebox-macos"
      End
    End

    Context "when OS is Linux on Intel CPU"
      Mock 'uname'
        echo "Linux"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "x86_64"
      }

      It 'returns correct URL'
        When call get_download_url "${ASDF_INSTALL_VERSION}" 'kubebox'
        The output should equal "https://github.com/astefanutti/kubebox/releases/download/v${ASDF_INSTALL_VERSION}/kubebox-linux"
      End
    End

    Context "when OS is Linux on ARM CPU"
      Mock 'uname'
        echo "Linux"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "arm"
      }

      It 'returns correct URL'
        When call get_download_url "${ASDF_INSTALL_VERSION}" 'kubebox'
        The output should equal "https://github.com/astefanutti/kubebox/releases/download/v${ASDF_INSTALL_VERSION}/kubebox-arm"
      End
    End
  End
End
