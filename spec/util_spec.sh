
export ASDF_INSTALL_VERSION=123
export ASDF_INSTALL_PATH=/tmp

Describe 'asdf-kubebox'
  Include 'bin/install'

  Describe 'get_arch()'
    Context "when CPU is Intel"
      Mock 'uname'
        echo "x86_64"
      End

      It "returns x86_64"
        When call get_arch
        The output should equal "x86_64"
      End
    End

    Context "when CPU is ARM64"
      Mock 'uname'
        echo "arm64"
      End

      Context "and OS is Linux"
        # Have to cheat here as can not mock uname twice
        get_raw_platform() {
          echo "linux"
        }

        It "returns arm"
          When call get_arch
          The output should equal "arm"
        End
      End

      Context "and OS is OSX"
        # Have to cheat here as can not mock uname twice
        get_raw_platform() {
          echo "darwin"
        }

        It "returns arm"
          When call get_arch
          The output should equal "arm"
        End
      End
    End

    Context "when CPU is ARM"
      Mock 'uname'
        echo "armv7l"
      End

      It "returns arm"
        When call get_arch
        The output should equal "arm"
      End
    End
  End

  Describe 'get_platform()'
    Context "when OS is OSX"
      Mock uname
        echo "Darwin"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "x86_64"
      }

      It "returns macos"
        When call get_platform
        The output should equal "macos"
      End
    End

    Context "when OS is Linux on Intel CPU"
      Mock uname
        echo "Linux"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "x86_64"
      }

      It "returns linux"
        When call get_platform
        The output should equal "linux"
      End
    End

    Context "when OS is Linux on ARM CPU"
      Mock uname
        echo "Linux"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "arm"
      }

      It "returns arm"
        When call get_platform
        The output should equal "arm"
      End
    End

    Context "when OS is Windows"
      Mock uname
        echo "Windows"
      End

      # Have to cheat here as can not mock uname twice
      get_arch() {
        echo "x86_64"
      }

      It "returns windows.exe"
        When call get_platform
        The output should equal "windows.exe"
      End
    End
  End
End


